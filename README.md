# Techi Blogs

### 1. [JQuery Interview Questions and Answers](https://www.techgeekbuzz.com/top-jquery-interview-questions/)

* Explain jQuery in brief?
* Explain different features of jQuery?
* What are the Benefits of jQuery?
* Name the latest version of jQuery.
* Describe selectors in jQuery?
* How many types of selectors are there in jQuery?
* Define jQuery .noconflict?
* What are the Ajax functions available in jQuery?
* What are the several methods used to deliver the effects?
* What is .empty() versus .remove() versus .detach() in jQuery?

### 2. [Node.Js Interview Questions](https://www.techgeekbuzz.com/top-node-js-interview-questions/)

* What can you tell me about Node.js?
* Highlight some of the key features of Node.js?
* Is Node.js completely based on a single thread?
* What does event-driven programming mean?
* Define “I/O” operations?
* How Node.js works?
* Name the various API functions supported by Node.js?
* What do you understand by the control flow functions?
* Explain NPM.
* What are the two arguments that async.queue takes as input?
* State the difference between Node.js vs Ajax?
* Describe the advantages and disadvantages of Node.js?

### 3. [Docker Interview Questions and Answers](https://www.techgeekbuzz.com/top-docker-interview-questions/)

* What do you understand by the term Docker?
* What do you understand by virtualization?
* Name the latest version of Docker.
* What do you understand by containerization?
* Explain hypervisors used in VMs in simple terms.
* Differentiate containerization and virtualization.
* Explain Docker containers in simple terms.
* Explain Docker Images in simpler terms.
* What do you understand by Docker hub?
* Explain in simple terms the architecture of Docker.
* Explain the uses of a Dockerfile in Docker.

### 4. [Linux Interview Questions](https://www.techgeekbuzz.com/top-linux-interview-questions/)

* What is the latest version of Linux?
* What can you tell us about Linux?
* Can you explain what is Bash?
* Can you differentiate Unix and Linux?
* Explain CLI in Linux.
* Explain in your own words the use of Linux Kernel.
* Explain Kdump in Linux.
* Explain Linux Loaded.
* What is Swap Space in Linux?
* Can you explain Virtual Desktops?
* What is Root Account in Linux?
* Explain GUI in Linux.
* Mention Linux File Permissions.

### 5. [DevOps Interview Questions](https://www.techgeekbuzz.com/devops-interview-questions/)

* What can you tell me about DevOps?
* What is the need for DevOps in the current market?
* Explain the difference between DevOps and Agile in SDLC? (DevOps Vs Agile)
* List a few tools that might be handy for end-to-end DevOps.
* Explain how these tools work together to carry out DevOps practices?
* Enlist the advantages and benefits that DevOps brings to the table.
* What are some of the anti-patterns in DevOps?
* Explain the different phases in DevOps?
* Differentiate the terms Continuous Delivery (CD) and Continuous Deployment with respect to DevOps?
* Explain the role of Continuous Monitoring in DevOps?
* In DevOps, explain the purpose of Configuration Management (CM)?
* Explain the role of Amazon Web Services (AWS) in DevOps?

### 6. [Best Linux OS](https://webhostingprime.com/best-linux-os/)

There is a gigantic rundown of Linux circulations that are accessible available. So in case you are going to attempt Linux OS for the absolute first time, then, at that point, it's an extreme call to make. The Linux operating framework has specific intricacies of which you probably won't know as a beginner. 

You might discover the OS captivating from the get go, yet sorting out its components and capacities will take significantly additional time and information. 

In case you are wanting to turn on Linux for reasons unknown, you ought to essentially realize where to begin. The principal thing you should know is that Linux is anything but a solitary operating framework, however it has more than 100 dispersions. In view of useability, highlights, and obviously, your prerequisites, you can pick and introduce a specific Linux conveyance on your machine. 

Discussing standards, there are Linux dispersions exceptionally intended for novices. Furthermore, in this post, we will discuss the 10 best Linux operating systems that are direct to utilize regardless of whether you are a novice. Look through and get hold of our rundown now.

### 7. [Artificial Intelligence Interview Questions](https://www.techgeekbuzz.com/artificial-intelligence-interview-questions/)

1. Is AI good or bad?
2. Do you think machines will surpass humans? What will happen if this becomes true in the future?
3. What are the AI technologies that you use every day?
4. Do you believe that self-driving cars will work like human driving someday?
5. Which one would you like to pursue – AI or data science – as a career choice?
6. What do you understand about AI? Give a simple example.
7. If you were to build an AI system, what would you like to build?
8. How is AI different from machine learning?
9. What is the association between AI and deep learning?
10. What is deep learning?
11. How is AI related to data science?
12. Explain the layers of a neural network.
13. Explain the different branches (domains) of AI.
14. How many types of AI are there? Which is the most popular one?
15. What is fuzzy logic? Give some applications of fuzzy logic.

### 8. [Front End Developer Interview Questions](https://www.techgeekbuzz.com/best-front-end-interview-questions/)

1. What does DOCTYPE stand for, and what does it do?
2. What is the difference between null and undefined?
3. What is Lazy Loading?
4. What is Coercion in JS?
5. What is the use of meta tags in HTML documents?
6. What is Variable Scope in JS?
7. What is Node.js?
8. What is npm?
9. How does the server hanger the page in which content is present in multiple languages?
10. Why did we use the data- the attribute in HTML and why it is now discouraged to use?
11. What is IIFE?
12. What are the callback functions in JS?
13. What is React.js?
14. Why it is encouraged to use external JS and CSS over in-line?
15. What does this keyword do in JS?

### 9. [Solarmovie Alternatives in 2021](https://www.techgeekbuzz.com/top-solarmovie-alternatives/)

1. CMovies
2. 123Movies
3. Vumoo
4. YesMovies
5. YifyMovies
6. Movie4u
7. MovieWatcher
8. VexMovies
9. M4UFree
10. Putlockerwatch2

### 10. [Best book to learn Machine Learning](https://www.techgeekbuzz.com/best-machine-learning-books/)

Machine learning is probably the most blazing subject of 2021. It is basically a part of AI (Artificial Intelligence) that gives any machine the capacity to settle on choices with no human intercession or express programming. Machine learning calculations are worked by gathering gigantic measures of information from different sources and building a smart framework (model) in view of past encounters, designs and comparable practices.

![Image description](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/s42ik8pmey15usm44397.png) 

## List of best books:

*1. Hands-on ML with Scikit-learn, Keras, and TensorFlow* - [Buy](https://www.amazon.com/Hands-Machine-Learning-Scikit-Learn-TensorFlow/dp/1492032646/)

*2. Learning for absolute beginners* - [Buy](https://www.amazon.com/Machine-Learning-Absolute-Beginners-Introduction/dp/1549617214/)

*3. Machine learning: An applied mathematics introduction* - [Buy](https://www.amazon.com/Machine-Learning-Applied-Mathematics-Introduction/dp/1916081606/)

*4. Deep learning (Adaptive computation)* - [Buy](https://www.amazon.com/Deep-Learning-Adaptive-Computation-Machine/dp/0262035618/)

*5. Pattern Recognition* - [Buy](https://www.amazon.com/Pattern-Recognition-Learning-Information-Statistics/dp/0387310738/)

*6. Python Machine Learning — Second Edition* - [Buy](https://www.amazon.com/Python-Machine-Learning-scikit-learn-TensorFlow-ebook/dp/B0742K7HYF/)

*7. Machine learning for dummies* - [Buy](https://www.amazon.com/Machine-Learning-Dummies-John-Mueller/dp/1119245516)

*8. Fundamentals of ML for Predictive Data Analytics* - [Buy](https://www.amazon.com/Fundamentals-Machine-Learning-Predictive-Analytics/dp/0262029448/)

*9. Deep Learning with Python* - [Buy](https://www.amazon.com/Deep-Learning-Python-Francois-Chollet/dp/1617294438/)

*10. A Probabilistic Perspective* - [Buy](https://www.amazon.com/Machine-Learning-Probabilistic-Perspective-Computation/dp/0262018020/)


### Conclusion:

You can decide to purchase whatever blend of Machine Learning books you need to, in light of the data given in this article. I would suggest you start with Machine learning for outright fledglings, trailed by some other middle of the road or another essential level book in the event that you have no clue about ML or Python. In the event that you know math or Python or a tad of ML, ML for fakers or even Fundamentals of Machine Learning for Predictive Data Analytics would be a decent decision to start.

### 11. [Python Interview Questions](https://www.techgeekbuzz.com/python-interview-questions/)

* Name some main features of Python for which it is popular.
* What is the latest version of Python?
* How Python is a dynamically typed language?
* Name all the inbuilt data types in Python.
* What are sets in Python?
* Why this statement often used in python: if __name__==”__main__”: ?
* What is Python GIL?
* What is polymorphism in Python?
* What is encapsulation in Python?
* How the Python list is different from a linked list.
* What will be the output of the following code?
* What would be the output of this code?
* What would be the output?
* What would be the output of the following code?
* What would be the output of the following code?

### 12. [Angular Interview Questions](https://www.techgeekbuzz.com/angular-interview-questions/)

* What is the purpose of Angular?
* What are templates in Angular?
* How are Angular expressions different from JavaScript expressions?
* Explain the architecture of Angular.
* Tell us some differences between components and directives.
* What is the primary language used in Angular?
* What is the latest version of Angular?
* What are NgModules? Differentiate between Javascript modules and NgModules.
* What are ngIf and ngFor? Can you show a small example to use them?
* What are the new features in the latest version of Angular?
* What is the digest cycle?

### 13. [PHP Interview Questions](https://www.techgeekbuzz.com/php-interview-questions/)

* What is PHP?
* What is the full form of PHP?
* What does PEAR stand for?
* Which programming language does PHP resemble?
* What are the differences between PHP & JAVA?
* What is the latest version of PHP?
* What is the command to execute a PHP script from the command line?
* Which function is used to encrypt the password in PHP?
* Write a Hello World program using the variable?
* Define the Zend Engine?
* Write the command to run the interactive PHP shell from the CLI (command-line interface)?

### 14. [Java interview questions](https://www.techgeekbuzz.com/core-java-interview-questions/)

* What is Java?
* What is Core Java?
* What is JVM?
* How does a bytecode run in JAVA?
* Why do we declare Main as Static?
* When a subclass has only parameterized constructor and a superclass does not have a matching constructor, What will happen?
* Explain Public Static Void Main (String args[]).
* What do you understand by a Wrapper class?
* Why JAVA is a statically typed language?
* What is Package in JAVA?
* What is the Java language programming platform?
* Name all Java Programming language platforms.
* Explain Method Overloading and Overriding.
* What is an interface in Java?

### 15. [SQL Interview Questions](https://www.techgeekbuzz.com/sql-interview-questions/)

* What is Data?
* What is a Database?
* What is SQL?
* What is the latest version of SQL?
* What is DBMS?
* Name the different types of DBMS.
* Give some Key Features of SQL
* What is RDBMS?
* How DBMS is different from RDBMS?
* Name some RDBMS software that uses SQL.
* What is a Table in SQL?
* What Does a JOIN clause do in SQL?
* Name the different types of JOINS in SQL
* How SQL is different from a programming language?
* Name the different Datatypes of SQL.

### 16. [Best Linux Distros](https://www.techgeekbuzz.com/linux-distribution/)

![Linux Distributions](https://user-images.githubusercontent.com/90754617/138697415-1789da54-6b28-42ff-bb45-dda51c4ffb60.png)

* The Linux kernel
* GNU tools
* Libraries
* Additional software
* APIs
* A window system
* A window manager
* A desktop environment and
* Documentation.

### 17. [Python Program to Add Two Matrices](https://www.techgeekbuzz.com/python-program-to-add-two-matrices/)

A matrix is simply defined as some arrangement of numbers that is formed by using columns and rows. In Python, we use a list of lists to represent a matrix.

If each element of the list is itself a list then it would be a matrix. For instance, [[1,2,3],[3,5,3], [7,9,10]] is a 3×3 matrix i.e. it has 3 rows and 3 columns.

Matrix Addition
The number of rows and columns in a matrix determines its dimensions. For example, [2, 3, 4, 5, 22, 24] is a one-dimensional array while [[2,3], [4,5], [22,24]] is a 2-dimensional array. Following is an example of a 3×3 matrix:

[

[1,2,3],

[3,5,3],

[7,9,10]

]

To add two matrices their dimensions should be similar. For example, you can add a 2×3 matrix only with another matrix with 2 rows and 3 columns.

### 18. [Public Domain Photos](https://webhostingprime.com/top-public-domain-images/)

![Domain Images](https://user-images.githubusercontent.com/90754617/139254414-8077f069-1818-44b9-a6a6-1878e5f769c7.png)

* Pixabay
* Public Domain Archive
* New Old Stock
* Gratisography
* Picjumbo
* Kaboompics
* Pexels
* Flickr Commons
* Morguefile
* Unsplash

### 19. [Best CSS Frameworks](https://www.techgeekbuzz.com/best-css-frameworks/)

* Bootstrap
* Foundation
* Pure
* Bulma
* Semantic UI
* Materialize CSS
* Skeleton
* Tailwind CSS
* UI Kit
* Ant Design

### 20. [Python GUI Frameworks](https://www.techgeekbuzz.com/best-python-gui-frameworks/)

![Python GUI Frameworks](https://user-images.githubusercontent.com/90754617/139427821-1a05d3af-5568-4d6f-a51a-71732ee87554.png)

* Kivy
* Tkinter
* PyQt
* PyGame
* WxPython

### 21. [Types of Data Structure](https://www.techgeekbuzz.com/types-of-data-structure/)

* **Primitive**
  * Integer
  * (Double) Floating-point Numbers
  * Character
  * Pointer
* **Non-primitive**
  * Linear Data Structure
    * Array
    * List
    * Stack
    * Queue
  * Non-Linear Data Structure
     * Tree
     * Graph
  * File Data Structure

### 22. [Types of Operating Systems](https://www.techgeekbuzz.com/types-of-operating-systems/)

* Batch Operating System
* Interactive Operating System
* Real-Time Operating System
* Multiprogramming Operating System
* Multitasking/Time-sharing Operating System
* Multiprocessing Operating System
* Distributed Operating System

### 23. [Python Matrix](https://www.techgeekbuzz.com/python-matrix/)

**What is a Matrix:**

In general, a matrix is a mathematical concept to represent equations in a 2-D structure, using rows and columns.

**Python Matrix:**

# list approach to make a Matrix

```
matrix = [[1,2,3,4,5],
          [6,7,8,9,10],
          [11,12,13,14,15]
         ]
```

**NumPy Arrays:**

**Example:**
```
from numpy import array
arr = array([2,4,8,12,26])
print(arr)
print(type(arr)
```

**Output:**
```
[ 2  4  8 12 26]
<class 'numpy.ndarray'>
```

**Matrix Addition:**

We can only add two matrices if they have the same number of rows and columns. To add two matrices we can use the Arithmetic “ +” operator.

**Example:**
```
import numpy as np
A = np.array([ [1,2,3], [4,5,6], [7,8,9]])
B = np.array([[7,8,9],[4,5,6],[1,2,3]])
print("Matrix Addtion")
print(A+B)
```

**Output:**

**Matrix Addtion**
```
[[ 8 10 12]
 [ 8 10 12]
 [ 8 10 12]]
```

